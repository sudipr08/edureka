package readwrite;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import constantvalues.ConstantValues;

public class Readandwriteusinfexcelsheet {
	
	static XSSFWorkbook wb;
	static XSSFSheet ws;
	static XSSFRow rownumberfromexcel;
	static XSSFCell cellfromexcel;
	
	public static void giveexcelPath(String excelpath) throws Exception {
		File f = new File(excelpath);	
		FileInputStream fis = new FileInputStream(f);
		wb = new XSSFWorkbook(fis);
	}
	
	public static String readvalues(String sheetname, int roNum, int coNum) {
		ws = wb.getSheet(sheetname);
		DataFormatter df = new DataFormatter();
		cellfromexcel = ws.getRow(roNum).getCell(coNum);
		String cellvalue = df.formatCellValue(cellfromexcel);
		return cellvalue;
	}
	
	
	public static void writevalues(String sheetname, int roNum, int coNum, String testdata) throws Exception {
		ws = wb.getSheet(sheetname);
		cellfromexcel = ws.getRow(roNum).getCell(coNum);
		cellfromexcel.setCellValue(testdata);
		
		FileOutputStream fos = new FileOutputStream(new File(ConstantValues.excelPath));
		wb.write(fos);
		fos.close();
		
		FileInputStream fis = new FileInputStream(new File(ConstantValues.excelPath));
		wb = new XSSFWorkbook(fis);
		
	}

}
