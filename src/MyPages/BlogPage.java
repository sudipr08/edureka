package MyPages;

import java.util.ArrayList;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


import CommonScript.CommonMethods;


public class BlogPage extends CommonMethods{
	
	
	@FindBy (how = How.CSS, using= "div.navbar-collapse.collapse.navbar-right.clpnavbarhead ul li:nth-child(4) div.dropdown.menu-dropdown")
	static WebElement Click_Community_Dropdown;
	
	@FindBy (how = How.XPATH, using= "//li[@class='ga-blog ga_ecom_info']")
	static WebElement Open_Blogpage;
	
	@FindBy (how = How.CSS, using= "#search-inp")
	static WebElement BlogPage_Search;
	
	@FindBy (how = How.CSS, using= "#margin-top-value > section.container-fluid.blog_menu_background > div > div > div > div > div.col-lg-9.col-md-9.col-sm-8.col-xs-12 > ul > li:nth-child(3) > a")
	static WebElement BlogPage_Click_InterviewQuestion;

	
	public static void openBlogPage() {
		
		PageFactory.initElements(driver, BlogPage.class);
		
		System.out.println("OPENING COMMUNITY DROPDOWN!!!!");		
		CommonMethods.elementWaitVisOF(Click_Community_Dropdown);
		Click_Community_Dropdown.click();
		
		System.out.println("OPENING BLOG PAGE!!!!");
		CommonMethods.elementWaitVisOF(Open_Blogpage);
		Open_Blogpage.click();
		
//		wait = new WebDriverWait(driver, 30);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='dropdownMenu4']")));
//		driver.findElement(By.xpath("//a[@id='dropdownMenu4']")).click();
		
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='ga-blog ga_ecom_info']")));
//				
//		driver.findElement(By.xpath("//li[@class='ga-blog ga_ecom_info']")).click();
		 
	}
	
	public static void switchTabs() {
		ArrayList<String> tabs = new ArrayList <String>(driver.getWindowHandles());
	    System.out.println("Number of Tabs!!! === " + tabs.size());
	    System.out.println("Switching to parent Tab!!!");
//	    driver.close();
	    driver.switchTo().window(tabs.get(0)); 
	}
	
	public static void search() {
		
		ArrayList<String> tabs = new ArrayList <String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		
//		wait = new WebDriverWait(driver, 30);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#search-inp")));
//		System.out.println("ENTERING IN THE SEARCHBOX!!!!");
//		driver.findElement(By.cssSelector("#search-inp")).sendKeys("selenium");
//		driver.findElement(By.cssSelector("#search-inp")).sendKeys(Keys.ENTER); 
		
		CommonMethods.elementWaitVisOF(BlogPage_Search);
		BlogPage_Search.sendKeys("selenium");
		BlogPage_Search.sendKeys(Keys.ENTER);
		
	}
	
	public static void clickInterviewQuestions() {
		System.out.println("CLICKING INTERVIEW QUESTIONS!!!");
		CommonMethods.elementWaitCickOF(BlogPage_Click_InterviewQuestion);
		BlogPage_Click_InterviewQuestion.click();
		
		
//		wait = new WebDriverWait(driver, 30);
//		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#margin-top-value > section.container-fluid.blog_menu_background > div > div > div > div > div.col-lg-9.col-md-9.col-sm-8.col-xs-12 > ul > li:nth-child(3) > a")));
//		
//		driver.findElement(By.cssSelector("#margin-top-value > section.container-fluid.blog_menu_background > div > div > div > div > div.col-lg-9.col-md-9.col-sm-8.col-xs-12 > ul > li:nth-child(3) > a")).click();

		
	}

}
