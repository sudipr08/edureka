package MyPages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import CommonScript.CommonMethods;


public class MyCoursePage extends CommonMethods{
	
	
	
	@FindBy(how = How.CSS, using="div.navbar-collapse.collapse.navbar-right.clpnavbarhead ul li:nth-child(3) div a")
	static WebElement openCoursesDropdown;
	
	@FindBy(how = How.CSS, using="div.navbar-collapse.collapse.navbar-right.clpnavbarhead ul li:nth-child(3) div ul li:nth-child(1)")
	static WebElement openMyClassroom;
	
	@FindBy (how = How.CSS, using = "#search-inp")
	static WebElement AllCourse_Page_Search;
	
	@FindBy (how = How.CSS, using = "#d_wishlist-icon-535")
	static WebElement AllCourse_Page_Mark_fav;
	
	@FindBy (how = How.CSS, using = "#header-II > section > nav > div > ul.nav.navbar-nav.navbar-right.profile.pull-right > li.dropdown.userdrop.hidden-sm.hidden-xs > a")
	static WebElement MyProfile_Dropdown;
	
	@FindBy (how = How.CSS, using = "#header-II > section > nav > div > ul.nav.navbar-nav.navbar-right.profile.pull-right > li.dropdown.userdrop.hidden-sm.hidden-xs.open > ul > li:nth-child(5)")
	static WebElement MyProfile_MyWishlist;
	
	@FindBy(how = How.XPATH, using="//div[@class='btn-group batch_type_dd']")
	static WebElement textToBePresentInElementLocated;
	
	@FindBy(how = How.XPATH, using="//a[@class='dropdown-item'][contains(text(),'All')]")
	static WebElement clickAllDropdownOption;
	@FindBy(how = How.XPATH, using="//div[@class='btn-group batch_type_dd']")
	static WebElement openFilterDropdown;
	

	
	public static void openMyClassroom() {
		
		elementWaitCickOF(openCoursesDropdown);		
		System.out.println("OPENING COURSES DROPDOWN!!!!");	
		openCoursesDropdown.click();
		
		System.out.println("OPENING MY CLASSROOM!!!!");	
		
		elementWaitVisOF(openMyClassroom);
		openMyClassroom.click();
				
	}
	
	public static void searchSelenium() {
		PageFactory.initElements(driver, MyCoursePage.class);
			
		elementWaitVisOF(AllCourse_Page_Search);
			
			AllCourse_Page_Search.sendKeys("selenium");
			AllCourse_Page_Search.submit();
			
	
			System.out.println("SEARCHED SELENIUM");
	}
	
	public static void markFav() {
		
		elementWaitVisOF(AllCourse_Page_Mark_fav);
		AllCourse_Page_Mark_fav.click();		

		System.out.println("Marked Favorite Course");
	}
	
	public static void verifyWishlistPage() {
		
		MyProfile_Dropdown.click();
		MyProfile_MyWishlist.click();

		System.out.println("Verified My Wishlist Page!!!! ");
	}

	public static void verifyEnrollment() {
		
		elementWaitVisOF(openCoursesDropdown);		
		openCoursesDropdown.click();
		
		elementWaitVisOF(openMyClassroom);
		openMyClassroom.click();
		
		elementWaitVisOF(openFilterDropdown);		
		openFilterDropdown.click();
		clickAllDropdownOption.click();
		
		CommonMethods.elementWaitVisOF(textToBePresentInElementLocated);		
		if(driver.getPageSource().contains("Batch Selection Pending")) {
		    System.out.println("ENROLLED!!!! \nEnrollment Status: Batch Selection Pending".toUpperCase());
		} 
		if(driver.getPageSource().contains("Ongoing")) {
			System.out.println("ENROLLED!!!! \ncurrent course: ONGOING!!!!".toUpperCase());
		} else {
			System.out.println("Enrollment Status: Not enrolled!!!!".toUpperCase());
		}
		
	}	

}
