package MyPages;


import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import CommonScript.CommonMethods;

public class AllCoursePage extends CommonMethods{

	

	@FindBy (how = How.CSS, using="div.navbar-collapse.collapse.navbar-right.clpnavbarhead ul li:nth-child(3) div a")
	static WebElement Open_Courses_Dropdown;
	
	@FindBy (how = How.CSS, using="div.dropdown.menu-dropdown.open > ul > li:nth-child(2)")
	static WebElement Open_Courses_AllCourses;
	
	
	public static void openAllCoursesPage() {
		
		PageFactory.initElements(driver, AllCoursePage.class);
		ArrayList<String> tabs = new ArrayList <String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));
		
		elementWaitCickOF(Open_Courses_Dropdown);
		Open_Courses_Dropdown.click();
	
		CommonMethods.elementWaitVisOF(Open_Courses_AllCourses);
		Open_Courses_AllCourses.click();
		
		System.out.println("OPENING ALL COURSES PAGE!!!!");		

		
		 
	}

}
