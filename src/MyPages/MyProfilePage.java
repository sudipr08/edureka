package MyPages;

import java.util.ArrayList;
import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import CommonScript.CommonMethods;

public class MyProfilePage  extends CommonMethods{
	
	
	@FindBy (how = How.CSS, using="div.container div.pull-right.url_list ul.nav li:nth-child(8) div.btn-group button.dropdown-toggle")
	static WebElement MyProfile_Click_Username;
	
	@FindBy (how = How.CSS, using="div.container div.pull-right.url_list ul.nav li:nth-child(8) div.btn-group ul li:nth-child(1)")
	static WebElement MyProfile_Open;
	
	@FindBy (how = How.CSS, using="div.personal-detail-tab form.personal-details-form.ng-untouched.ng-pristine.ng-valid button.btn.btn-default.pull-right.verify-continue-btn")
	static WebElement MyProfile_PersonalDetails_Continue_Button;
	
	@FindBy (how = How.CSS, using="ul.nav.nav-tabs.nav-stacked.leftnav-tabs > li:nth-child(4).onboarding-tabs.professional-details-tab")
	static WebElement MyProfile_clickProfessionalDetailsPage;
	
	@FindBy (how = How.NAME, using="companyName")
	static WebElement MyProfile_ProfessionalDetailsPage_CompanyName;
	
	@FindBy (how = How.CSS, using="#onboarding > div > div.col-lg-12.col-md-12.col-sm-12.col-xs-12.main-body > div.col-lg-9.col-md-9.col-sm-12.col-xs-12.tab-data > div.tab-content > app-onboarding-professional-details > accordion > accordion-group > div > div.panel-collapse.collapse.in.show > div > form > div.form-group.form-div.col-md-6.skills > input")
	static WebElement MyProfile_ProfessionalDetails_Skills;
	
	@FindBy (how = How.NAME, using="currentjob")
	static WebElement MyProfile_ProfessionalDetails_JobLevel;
	
	@FindBy (how = How.CSS, using="select[name*='currentIndustry']")
	static WebElement MyProfile_ProfessionalDetails_CurrentIndustry;
	
	@FindBy (how = How.CSS, using= "div.form-group.form-div.col-md-6.current-job > select > option:nth-child(3)")
	static WebElement MyProfile_ProfessionalDetails_CurrentIndustry_Dropdown;
	
	@FindBy (how = How.CSS, using= "form.professional-details-form button.onboarding-primary-button")
	static WebElement MyProfile_ProfessionalDetails_NextButton;
	
	@FindBy (how = How.CSS, using= "form.career-interest-form button.onboarding-primary-button")
	static WebElement MyProfile_CareerInterest_NextButton;
	
	@FindBy (how = How.CSS, using= "div > form > div.finish-button-div.hidden-xs button.btn.pull-right.onboarding-primary-button")
	static WebElement MyProfile_OtherDetails_SaveButton;
	
	@FindBy (how = How.CSS, using= "#personal_details > i")
	static WebElement MyProfile_ClickEditProfile;
	
	@FindBy (how = How.CSS, using= "div.editloader")
	static WebElement loadSpinner;

	@FindBy (how = How.CSS, using= "span.changes-saved")
	static WebElement saveConfirmation;

	@FindBy (how = How.CSS, using= "iframe#wiz-iframe-intent")
	static WebElement promoFrame;

	@FindBy (how = How.CSS, using= "span.CT_InterstitialClose")
	static WebElement ClosePromoFrame;

	ArrayList<String> tabs = new ArrayList <String>(driver.getWindowHandles());

	public void switchTabs() {
	    System.out.println("Number of Tabs!!! === " + tabs.size());
	    System.out.println("Switching to parent Tab!!!");
	    driver.switchTo().window(tabs.get(0)); 
	}

	public void switchToPromoframe() {
	    driver.switchTo().frame(promoFrame);
	    ClosePromoFrame.click();
	    driver.switchTo().window(tabs.get(0)); 
	}

	public void closePromoFrame() {
		if (promoFrame.isDisplayed()) {
		switchToPromoframe();
		switchTabs();
		}
	}

	public static void openMyProfilePage() {
		PageFactory.initElements(driver, MyProfilePage.class);
	
		
		CommonMethods.elementWaitVisOF(MyProfile_Click_Username);
		MyProfile_Click_Username.click();
		
		CommonMethods.elementWaitVisOF(MyProfile_Open);
		MyProfile_Open.click();
		System.out.println("MY PROFILE PAGE OPENED!!!!");
		
	}	
	public static void clickEditProfile() {
		MyProfile_ClickEditProfile.click();
		System.out.println("MY EDIT PROFILE PAGE OPENED!!!!");
	}
	

	
	
	public static void clickProfessionalDetailsPage() {
		
		CommonMethods.elementWaitVisOF(MyProfile_clickProfessionalDetailsPage);
		MyProfile_clickProfessionalDetailsPage.click();
		System.out.println("PROFESSIONAL DETAILS PAGE OPENED !!!!");
	}
	public static void editProfessionalDetailsPage() {
		
		Random rand = new Random();
		int n = rand.nextInt(5) + 1;
		int j = rand.nextInt(15) + 1;
		
		
		
		CommonMethods.elementWaitVisOF(MyProfile_PersonalDetails_Continue_Button);
		MyProfile_PersonalDetails_Continue_Button.click();
		
		MyProfile_ProfessionalDetailsPage_CompanyName.clear();
		MyProfile_ProfessionalDetailsPage_CompanyName.sendKeys("TVO, Selenium, Practice");
		
        CommonMethods.elementWaitVisOF(saveConfirmation);

		MyProfile_ProfessionalDetails_Skills.clear();
		MyProfile_ProfessionalDetails_Skills.sendKeys("Office, Java, HTML, CSS, Agile");		
		
        CommonMethods.elementWaitVisOF(saveConfirmation);

        
        CommonMethods.selectByIndex(MyProfile_ProfessionalDetails_CurrentIndustry, j);
        CommonMethods.selectByIndex(MyProfile_ProfessionalDetails_JobLevel, n);
      	
        CommonMethods.elementWaitVisOF(saveConfirmation);

        
        CommonMethods.elementWaitVisOF(MyProfile_ProfessionalDetails_NextButton);
        MyProfile_ProfessionalDetails_NextButton.click();
		
        CommonMethods.elementWaitVisOF(MyProfile_CareerInterest_NextButton);
        MyProfile_CareerInterest_NextButton.click();
        
        CommonMethods.elementWaitVisOF(MyProfile_OtherDetails_SaveButton);
        MyProfile_OtherDetails_SaveButton.click();
        
		System.out.println("PROFESSIONAL DETAILS PAGE EDITED !!!! \nN value is  " + n + "\nJ value is: " + j);
	}

}
