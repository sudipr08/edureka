package Test;



import CommonScript.DriverScript;
import MyPages.AllCoursePage;
import MyPages.BlogPage;
import MyPages.MyCoursePage;
import MyPages.MyProfilePage;

public class TestPage extends DriverScript {
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

//		setConnection("FIREFOX");//
//		MyCoursePage.openEdureka();
//		MyCoursePage.logIn();
//		BlogPage.openBlogPage();
//		BlogPage.search();
//		BlogPage.clickInterviewQuestions();
//		BlogPage.switchTabs();
//		AllCoursePage.openAllCoursesPage();
//		MyCoursePage.searchSelenium();
//		MyCoursePage.markFav();
//		MyCoursePage.verifyWishlistPage();
//		MyCoursePage.verifyEnrollment();
//		MyProfilePage.openMyProfilePage();
//		MyProfilePage.clickEditProfile();
//		MyProfilePage.editProfessionalDetailsPage();
//		MyCoursePage.Logout(); 
		

		setConnection("chrome");//
		MyCoursePage.openEdureka();
		MyCoursePage.logIn();
		BlogPage.openBlogPage();
		BlogPage.search();
		BlogPage.clickInterviewQuestions();
		BlogPage.switchTabs();
		AllCoursePage.openAllCoursesPage();
		MyCoursePage.searchSelenium();
		MyCoursePage.markFav();
		MyCoursePage.verifyWishlistPage();
		MyCoursePage.verifyEnrollment();
		MyProfilePage.openMyProfilePage();
		MyProfilePage.clickEditProfile();
		MyProfilePage.editProfessionalDetailsPage();
		MyCoursePage.Logout();  
				

	}

}
