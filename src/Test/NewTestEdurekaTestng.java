package Test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonScript.DriverScript;
import CommonScript.Logging;
import MyPages.AllCoursePage;
import MyPages.BlogPage;
import MyPages.MyCoursePage;
import MyPages.MyProfilePage;

public class NewTestEdurekaTestng {
  
  @BeforeTest
  public void beforeTest() {
	   DriverScript.setConnection("chrome");
  }
  
  @Test (priority=1)
  public void OpenEdureka() {	  
	  Logging.info("OpenEdureka");
	  	MyCoursePage.openEdureka();
		MyCoursePage.logIn();	  
  }
  
  @Test (priority=2)
  public void BlogPageTest() {	  
	  Logging.info("BlogPageTest");

	  	BlogPage.openBlogPage();
		BlogPage.search();
		BlogPage.clickInterviewQuestions();
		BlogPage.switchTabs();	  
  }
  
  @Test (priority=3)
  public void OpenAllCoursesPage() {
	  Logging.info("OpenAllCoursesPage");

	  AllCoursePage.openAllCoursesPage();
  }
  
  @Test (priority=4)
  public void MarkFavCourse() {
	  	MyCoursePage.searchSelenium();
		MyCoursePage.markFav();
		MyCoursePage.verifyWishlistPage();	  
  }
  
  @Test (priority=5)
  public void verifyEnrollment() {
	  MyCoursePage.verifyEnrollment();
  }
  
  @Test (priority=6)
  public void EditMyProfilePage() {
	  	MyProfilePage.openMyProfilePage();
		MyProfilePage.clickEditProfile();
		MyProfilePage.editProfessionalDetailsPage();
  }  
	  
  @AfterTest
  public void Logout() {
	  MyCoursePage.Logout();  
		
  }

}
