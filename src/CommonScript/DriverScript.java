package CommonScript;




import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;


public class DriverScript {
	protected static  WebDriver driver;
	
	protected static WebElement element = null;
	
	
	
	public static void setConnection(String browser) {
		String ck = browser.toUpperCase(); 
		if (ck.equals("CHROME")) {
			ChromeOptions options = new ChromeOptions();
//			options.addArguments("-incognito");
			options.addArguments("--disable-notifications"); 
			options.addArguments("--disable-popup-blocking");
//			options.addArguments("--disable-default-apps");
			//create the chrome connection. 
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Work\\Desktop\\workspace\\Edureka-FinalAssignment\\web drivers\\chromedriver_win32\\chromedriver.exe");
			//Open the Chrome browser. 
			driver = new ChromeDriver(options); 
			System.out.println("Starting Chrome Browser !!!".toUpperCase()); 
			Logging.info("Starting Chrome Browser !!!".toUpperCase());
			} else if (ck.equals("FIREFOX")) {
				FirefoxOptions options = new FirefoxOptions();
				options.addArguments("--disable-notifications");
				options.addPreference("dom.webnotifications.enabled", false);
				options.addArguments("--disable-popup-blocking");
				options.addPreference("privacy.popups.showBrowserMessage", false);
				System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true"); 
				System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
				System.setProperty("webdriver.gecko.driver", "C:\\Users\\Work\\Desktop\\workspace\\Edureka-FinalAssignment\\web drivers\\geckodriver-v0.23.0-win64\\geckodriver.exe"); 
				driver = new FirefoxDriver(options);
				System.out.println("Starting Firefox Browser !!!".toUpperCase()); 
				Logging.info("Starting Firefox Browser !!!".toUpperCase());
			}
	}
	
	public static WebDriver getDriver() {
		return driver;
	}
		
	
}


