package CommonScript;

import java.util.ArrayList;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import CommonScript.DriverScript;
import org.apache.log4j.*;

public class CommonMethods {

	protected static WebDriverWait wait = null ;
	protected static WebDriver driver = DriverScript.getDriver();
	
	@FindBy (how = How.XPATH, using="//a[@class='signin top-signin giTrackElementHeader']")
	static WebElement Open_Edureka_Login_Button;
	
	@FindBy (how = How.CSS, using="#si_popup_email")
	static WebElement LoginForm_email;
	
	@FindBy (how = How.CSS, using="#si_popup_passwd")
	static WebElement LoginForm_Password;
	
	@FindBy (how = How.CSS, using="#new_sign_up_mode > div > div > div.modal-body > div.login_video_frm.login-vd-box.sigin-new-division > form > button")
	static WebElement LoginForm_Submit_Button;
	
	@FindBy (how = How.CSS, using="#footauto > app-root > app-profile-main > app-header > header > nav > div > div.pull-right.url_list > ul > li.dropdown.user_drop > div")
	static WebElement OpenLogoutDropdown;
	
	@FindBy (how = How.CSS, using="#footauto > app-root > app-profile-main > app-header > header > nav > div > div.pull-right.url_list > ul > li.dropdown.user_drop > div > ul > li:nth-child(12) > a")
	static WebElement Logout_Link;


	public static void openEdureka() {
		
		driver.get("https://www.edureka.co/");
		driver.manage().window().maximize();  
		PageFactory.initElements(driver, CommonMethods.class);
		
	}

	public static void logIn () {

		//sroy@tvo.org, Komo2017
		System.out.println("TRYING 2 LOGIN !!!!"); 
		
		elementWaitVisOF(Open_Edureka_Login_Button);
		Open_Edureka_Login_Button.click();
		
		elementWaitVisOF(LoginForm_email);
		LoginForm_email.clear();
		LoginForm_Password.clear();
		LoginForm_email.sendKeys("sroy@tvo.org");
		LoginForm_Password.sendKeys("Komo2017");
		
		LoginForm_Submit_Button.click();
		
		System.out.println("LOGIN COMPLETED!!!!");
		

	}

	public static void Logout() {

		System.out.println("Logging Out!!!");
		
		elementWaitVisOF(OpenLogoutDropdown);
		
		OpenLogoutDropdown.click();
		Logout_Link.click();
		

		System.out.println("Logout Complete!!!");

		
		
		ArrayList<String> tabs = new ArrayList <String>(driver.getWindowHandles());
		System.out.println("CLOSING BROWSER !!!!" );
		
		for (int i = (int)(tabs.size())-1; i >= 0; --i) {

			if(i >0) {
				driver.switchTo().window(tabs.get(i));
				System.out.println("CLOSING TAB # " +(i+1)+" !!!");
				driver.close();
				
			} else {
				driver.switchTo().window(tabs.get(i));
				System.out.println("CLOSING TAB # " +(i+1)+" !!!");
				driver.quit();
			}
					
		}
	}
	
	public static void selectByIndex(WebElement element, int ind) {
		Select select = new Select(element);
        select.selectByIndex(ind);
        
	}
	
	public static void elementWaitVisOF(WebElement element) {
		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
        
	}
	public static void elementWaitCickOF(WebElement element) {
		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(element));
        
	}

	public static void waitForElementToBeInvisible(WebElement element) {
		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.invisibilityOf(element));
        
	}

}

